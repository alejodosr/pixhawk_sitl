/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

/*
* Extended Plugin for using both Optical Flow and LIDAR information (px4Flow)
* Author: Alejandro Rodríguez Ramos
* Date: 27th January, 2016
* Email: alejandro.dosr@gmail.com
*
* Description: This extention makes the Optical Flow plugin to publish its information
* via ROS Publisher.
*
* File: gazebo_px4Flow_plugin.cpp
*
*/

#ifdef _WIN32
  // Ensure that Winsock2.h is included before Windows.h, which can get
  // pulled in by anybody (e.g., Boost).
#include <Winsock2.h>
#endif

/*  Includes */

#include "gazebo/sensors/DepthCameraSensor.hh"
#include "gazebo_px4Flow_plugin.h"


#include <cv.h>
//#include <highgui.h>
#include <math.h>
#include <string>
#include <iostream>

using namespace cv;
using namespace std;

using namespace gazebo;


GZ_REGISTER_SENSOR_PLUGIN(CameraPlugin)



void image_process(void);

/////////////////////////////////////////////////
/// \brief CameraPlugin::CameraPlugin
///
CameraPlugin::CameraPlugin()
: SensorPlugin(), width(0), height(0), depth(0)
{
}

/////////////////////////////////////////////////
/// \brief CameraPlugin::~CameraPlugin
///
CameraPlugin::~CameraPlugin()
{
  this->parentSensor.reset();
  this->camera.reset();
  this->mutex2.unlock();
}

/////////////////////////////////////////////////
/// \brief CameraPlugin::Load
/// \param _sensor
/// \param _sdf
///
void CameraPlugin::Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf)
{
    // Exit if no ROS
    if (!ros::isInitialized())
    {
        gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
              << "properly initialized.  Try starting gazebo with ros plugin:\n"
              << "  gazebo -s libgazebo_ros_api_plugin.so\n";
        return;
    }

  if (!_sensor)
    gzerr << "Invalid sensor pointer.\n";

  this->parentSensor =
        #if GAZEBO_MAJOR_VERSION >= 7
          std::dynamic_pointer_cast<sensors::CameraSensor>(_sensor);
#else
          boost::dynamic_pointer_cast<sensors::CameraSensor>(_sensor);
#endif


  if (!this->parentSensor)
  {
    gzerr << "CameraPlugin requires a CameraSensor.\n";
#if GAZEBO_MAJOR_VERSION >= 7
    if (std::dynamic_pointer_cast<sensors::DepthCameraSensor>(_sensor))
#else
    if (boost::dynamic_pointer_cast<sensors::DepthCameraSensor>(_sensor))
#endif
      gzmsg << "It is a depth camera sensor\n";
  }

#if GAZEBO_MAJOR_VERSION >= 7
  this->camera = this->parentSensor->Camera();
#else
  this->camera = this->parentSensor->GetCamera();
#endif

  if (!this->parentSensor)
  {
      gzerr << "CameraPlugin not attached to a camera sensor\n";
      return;
  }

#if GAZEBO_MAJOR_VERSION >= 7
  this->width = this->camera->ImageWidth();
  this->height = this->camera->ImageHeight();
  this->depth = this->camera->ImageDepth();
  this->format = this->camera->ImageFormat();
#else

  this->width = this->camera->GetImageWidth();
  this->height = this->camera->GetImageHeight();
  this->depth = this->camera->GetImageDepth();
  this->format = this->camera->GetImageFormat();
#endif
  //  this->camera_name =this->camera->GetName();
  this->camera_name =_sdf->GetElement("cameraName")->Get<std::string>();

  ROS_INFO("%s", this->camera_name.c_str());

  if(!this->camera_name.compare("camera_cam")){
      ROS_INFO("[gazebo_optical_flow_plugin] This plugin is intended to work as a normal camera \n");
  }
  else if(!this->camera_name.compare("camera_flow")){
      ROS_INFO("[gazebo_optical_flow_plugin] This plugin is intended to work as a Optical Flow camera \n");
  }
  else{
      ROS_WARN("[gazebo_optical_flow_plugin] The purpose is not recognized.. plugin doing nothing \n");
  }


  if (_sdf->HasElement("robotNamespace"))
      namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
  else
      gzwarn << "[gazebo_optical_flow_plugin] Please specify a robotNamespace.\n";

  this->node_handle_ = new ros::NodeHandle(namespace_);


  if(!this->camera_name.compare("camera_flow")){

      //Subscribers
      this->estimated_pose_sub = this->node_handle_->subscribe("/px4flow/raw/lidar", 10, &CameraPlugin::estimatedPoseCallback, this);
      this->gyro_sub = this->node_handle_->subscribe("/drone5/mavros/imu/data", 10, &CameraPlugin::gyroCallback, this);
      this->clock_sub = this->node_handle_->subscribe("clock", 10, &CameraPlugin::getSimulationClockTime, this);


      // Optical Flow publisher
      this->opticalflow_pub =  this->node_handle_->advertise<nav_msgs::Odometry>("px4flow/raw/speeds", 10);

  }

  //  if(!this->camera_name.compare("camera_cam")){
  image_transport::ImageTransport it(*(this->node_handle_));
  this->camera_pub = it.advertise("camera/image", 1);
  //  }

  this->newFrameConnection = this->camera->ConnectNewImageFrame(
              boost::bind(&CameraPlugin::OnNewFrame, this, _1, this->width, this->height, this->depth, this->format));

  this->parentSensor->SetActive(true);

  this->mutex2.lock();

  this->image_process_thread = boost::thread(&CameraPlugin::image_process, this);

  //Initialization
  x_rate = 0;
  y_rate = 0;

  // Kalman Filter
  KF.init(4,2,0,CV_64F);
  KF.measurementMatrix = (cv::Mat_<double>(2,4) << 0, 0, 1, 0,
                                                  0, 0, 0, 1);
  cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-5));
  cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-3));
  cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1));
  cv::setIdentity(KF.transitionMatrix, cv::Scalar::all(1));
  measurement = cv::Mat::zeros(2, 1, CV_64F);
}

void CameraPlugin::image_process(void){
    current_time = ros::Time::now();
    blockSize = 5;
    minDistance = 1;
    qualityLevel = 0.001;
    useHarrisDetector = false;
    maxfeatures = 50;
    int meancount = 0;
    double dist = 0;

    cv::Mat samples(BUFFER_SIZE, 2,CV_64F);
    cv::Mat estimated;

    while(ros::ok()){
        if(newImageReceived()){
            getLastImageFromBuffer();

            if(prev_frame.empty())  continue;

            //GetHFOV gives fucking gazebo::math::Angle which you can not cast...
            const double Hfov = 0.366;
            const double focal_length_px = (frame.cols/2)/tan(Hfov/2);

            double pixel_flow_x_avg = 0.0;
            double pixel_flow_y_avg = 0.0;
#if GAZEBO_MAJOR_VERSION >= 7
            double rate = this->camera->RenderRate();
#else
            double rate = this->camera->GetRenderRate();
#endif
            if (!isfinite(rate))
                rate =  30.0;
            double dt = 1.0 / rate;

            cv::Mat uflow;

            if ((!prev_frame.empty()) && (x_rate != 0)){
                //                calcOpticalFlowPyrLK(prev_frame, frame, featuresPrevious, featuresCurrent, featuresFound, err, cv::Size(64,64));
                // Optical flow implemented with Farneback (found a bug in Lukas Kanade Matcher)
                calcOpticalFlowFarneback(prev_frame, frame, uflow, 0.5, 3, 15, 3, 5, 1.2, 0);





                cv::Scalar mean = cv::mean(uflow);
                cv::Mat flow = frame.clone();

                pixel_flow_x_avg = mean[1];   // Fitting AeroStack axes
                pixel_flow_y_avg = mean[0];

                // Discounting gyro rates
                double time_between_images = (double) (current_time.nsec - previous_time.nsec) / 1000000000 +
                        (double) (current_time.sec - previous_time.sec);

                KF.transitionMatrix.at<double>(0,2) = (double) time_between_images;
                KF.transitionMatrix.at<double>(1,3) = (double) time_between_images;

                double x_rate_pixel = current_x_rate * time_between_images * focal_length_px;
                double y_rate_pixel = current_y_rate * time_between_images * focal_length_px;

                double pixel_flow_x = pixel_flow_x_avg - x_rate_pixel;
                double pixel_flow_y = pixel_flow_y_avg - y_rate_pixel;

                // Compute saturation?
//                if(pixel_flow_x > SEARCH_SIZE)  pixel_flow_x = SEARCH_SIZE;
//                if(pixel_flow_y > SEARCH_SIZE)  pixel_flow_y = SEARCH_SIZE;

                //Compute speeds in adimensional/s
                double flow_compx = pixel_flow_x / focal_length_px / time_between_images;
                double flow_compy = pixel_flow_y / focal_length_px / time_between_images;

                //Compute speeds in m/s
                double new_velocity_x = flow_compx * current_altitude;
                double new_velocity_y = flow_compy * current_altitude;

                // Filter speed with the Kalman Filter
                cv::Mat prediction = KF.predict();
                measurement.at<double>(0) = new_velocity_x;
                measurement.at<double>(1) = new_velocity_y;
                cv::Mat correction = KF.correct(measurement);

                //Creating samples vectors
                if (meancount < BUFFER_SIZE){
                    meancount++;
                }
                if(meancount > 1){
                    //Shifting buffer
                    for (int i=1; i < meancount; i++){
                        samples.at<double>(i-1, 0) = samples.at<double>(i, 0);  // Column one: predicted value
                        samples.at<double>(i-1, 1) = samples.at<double>(i, 1);  // Column two: measurement
                    }
                }
                samples.at<double>(meancount-1, 0) = prediction.at<double>(2);
                samples.at<double>(meancount-1, 1) = new_velocity_x;

                if (meancount == BUFFER_SIZE){

//                    cout  << samples << endl;

                    // Calculating Covariance Matrix
                    cv::Mat Q,invQ, m;
                    cv::calcCovarMatrix(samples, Q, m, CV_COVAR_NORMAL+CV_COVAR_ROWS, CV_64F);
                    Q /= (samples.rows - 1);

//                    cout << Q << endl << endl;

                    //Inverting covariance Matrix
                    cv::invert(Q,invQ,DECOMP_SVD);

                    //Computing Mahalanobis distance
                    cv::Mat v1(1, 2, CV_64F);   // Current state
                    cv::Mat v2(1, 2, CV_64F);   // Previous state

                    v1.at<double>(0) = prediction.at<double>(2);
                    v1.at<double>(1) = new_velocity_x;

                    v2.at<double>(0) = samples.at<double>(meancount-2, 0);
                    v2.at<double>(1) = samples.at<double>(meancount-2, 1);


                    dist = cv::Mahalanobis(v1, v2, invQ);

//                    cout << dist << endl;


                    if(dist < THR_DIST)
                        estimated = correction.clone();
                    else
                        estimated = prediction.clone();
                }
                else    estimated = correction.clone();

                nav_msgs::Odometry sp;
                sp.header.stamp = time;
                sp.twist.twist.linear.x = estimated.at<double>(2);
                sp.twist.twist.linear.y = estimated.at<double>(3);
                sp.twist.twist.linear.z = correction.at<double>(2);//new_velocity_x; //debugging purposes

                opticalflow_pub.publish(sp);
            }
        }
    }
}

void CameraPlugin::getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){
    time = msg->clock;
}

void CameraPlugin::estimatedPoseCallback(const sensor_msgs::Range::ConstPtr& msg){
    altitude = msg->range;
}
void CameraPlugin::gyroCallback(const sensor_msgs::Imu::ConstPtr& msg){
    x_rate = msg->angular_velocity.x;
    y_rate = msg->angular_velocity.y;
    z_rate = msg->angular_velocity.z;

//    tf::Quaternion q(msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);
//    tf::Matrix3x3 m(q);
//    double roll, pitch, yaw;
//    m.getRPY(roll, pitch, yaw);

//    std::cout <<  roll << "\t" << pitch << std::endl;
}

bool CameraPlugin::newImageReceived(){
    mutex2.lock();
    return true;
}
void CameraPlugin::getLastImageFromBuffer(){
    mutex.lock();
    if (!frame.empty()){
        prev_frame = frame.clone();
        previous_time = current_time;
    }
    frame = img_buffer_ptr->image.clone();
    current_time = ros::Time::now();
    current_x_rate = x_rate;
    current_y_rate = y_rate;
    current_altitude = altitude;
    img_buffer_ptr.reset();
    mutex.unlock();
}


/////////////////////////////////////////////////
void CameraPlugin::OnNewFrame(const unsigned char * _image,
                              unsigned int _width,
                              unsigned int _height,
                              unsigned int _depth,
                              const std::string &_format)
{
  bool empty = false;
  mutex.lock();

  if(img_buffer_ptr.get() == 0) empty = true;

  try{
#if GAZEBO_MAJOR_VERSION >= 7
    _image = this->camera->ImageData(0);
#else
  _image = this->camera->GetImageData(0);
#endif
      Mat f = Mat(_height, _width, CV_8U);
      f.data = (uchar*)_image; //frame is not the right color now -> convert
      img_buffer_ptr = cv_bridge::toCvCopy(cv_bridge::CvImage(std_msgs::Header(), "mono8", f).toImageMsg(), sensor_msgs::image_encodings::MONO8);
  }
  catch(cv_bridge::Exception& e){
    ROS_ERROR("cv bridge exception: %s", e.what());
  }

  if(empty)   mutex2.unlock();

  mutex.unlock();

}
