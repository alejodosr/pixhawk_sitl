#include "gazebo_item_release.h"



using namespace std;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)

void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);

void ModelPush::itemReleasecb(const std_msgs::String::ConstPtr &msg){


    if (msg->data =="L"){
        if (joint_left_ != 0) {

#if GAZEBO_MAJOR_VERSION >= 7
                joint_left_->SetPosition(0, -2.1); //rad
#else
                joint_left_->SetAngle(0, -2.1); //rad

#endif
                ros::Duration(1).sleep();

#if GAZEBO_MAJOR_VERSION >= 7

                joint_left_->SetPosition(0, 0); //rad
#else
                joint_left_->SetAngle(0, 0); //rad
#endif


        }
    }

    if (msg->data =="R"){
        if (joint_right_ != 0) {

#if GAZEBO_MAJOR_VERSION >= 7
                joint_right_->SetPosition(0, -2.1); //rad
#else
                joint_right_->SetAngle(0, -2.1); //rad
#endif
                ros::Duration(1).sleep();

#if GAZEBO_MAJOR_VERSION >= 7
                joint_right_->SetPosition(0, 0); //rad
#else
                joint_right_->SetAngle(0, 0); //rad

#endif


        }
    }
}

gazebo::ModelPush modelPush;

/////////////////////////////////////////////////
/// \brief ModelPush::ModelPush
///
ModelPush::ModelPush()
{
}

/////////////////////////////////////////////////
/// \brief RayPlugin::~RayPlugin
///
ModelPush::~ModelPush()
{
}



void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model
        this->model = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

        // Exit if no ROS
        if (!ros::isInitialized())
        {
            gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
                  << "properly initialized.  Try starting gazebo with ros plugin:\n"
                  << "  gazebo -s libgazebo_ros_api_plugin.so\n";
            return;
        }

        if (_sdf->HasElement("robotNamespace"))
          namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
        else
          gzwarn << "[gazebo_moving_platform] Please specify a robotNamespace.\n";



        if (_sdf->HasElement("releaseJointName_left")) {
            //cout << "Aqui?" << endl;
            joint_name_left_ = _sdf->GetElement("releaseJointName_left")->Get<std::string>();
            joint_left_ = model->GetJoint(joint_name_left_);
        } else {
          joint_left_ = NULL;
        }

	if (_sdf->HasElement("releaseJointName_right")) {
            //cout << "Aqui?" << endl;
            joint_name_right_ = _sdf->GetElement("releaseJointName_right")->Get<std::string>();
            joint_right_ = model->GetJoint(joint_name_right_);
        } else {
          joint_right_ = NULL;

        }

        this->node_handle_ = new ros::NodeHandle(namespace_);


        this->release_sub = this->node_handle_->subscribe("arm_command/release", 10, &ModelPush::itemReleasecb, this);


        this->moving_sub = this->node_handle_->subscribe("clock", 10, getSimulationClockTime);

    }


void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
{
    // Get Sim time
    common::Time  time = common::Time::GetWallTime();
    double sec = modelPush.time_sec + modelPush.time_nsec / MILLION;
    //cout << sec << endl;

    double A = 4;
    double T = 4 * M_PI;
    double w = 2 * M_PI / T;
    double vel = A*w*cos(w*sec);



}


void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){

ros::Time time = msg->clock;

modelPush.time_sec = time.sec;
modelPush.time_nsec = time.nsec;


}
