#include "gazebo_truck.h"

using namespace std;
using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ModelPush)

void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);

gazebo::ModelPush modelPush;

/////////////////////////////////////////////////
/// \brief ModelPush::ModelPush
///
ModelPush::ModelPush()
{
}

/////////////////////////////////////////////////
/// \brief RayPlugin::~RayPlugin
///
ModelPush::~ModelPush()
{
}

void ModelPush::Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        // Store the pointer to the model
        this->model = _parent;

        // Listen to the update event. This event is broadcast every
        // simulation iteration.
        this->updateConnection = event::Events::ConnectWorldUpdateBegin(
                    boost::bind(&ModelPush::OnUpdate, this, _1));

        // Exit if no ROS
        if (!ros::isInitialized())
        {
            gzerr << "Not loading Optical Flow plugin since ROS hasn't been "
                  << "properly initialized.  Try starting gazebo with ros plugin:\n"
                  << "  gazebo -s libgazebo_ros_api_plugin.so\n";
            return;
        }

        if (_sdf->HasElement("robotNamespace"))
          namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>();
        else
          gzwarn << "[gazebo_moving_platform] Please specify a robotNamespace.\n";

        this->node_handle_ = new ros::NodeHandle(namespace_);

        this->moving_sub = this->node_handle_->subscribe("clock", 10, getSimulationClockTime);
        this->dronePositionPub   = this->node_handle_->advertise<droneMsgsROS::dronePositionRefCommandStamped>("/drone5/dronePositionRefs", 1);

        // Init
        this->nloops = -1;
        this->first = true;
        x_old = 0;
        y_old = 0;
        outFile.open ("/home/alejandro/mbzirc_speeds.csv", ios::out | ios::ate | ios::app) ;
        outFile << "time,x,y,vx,vy" << endl;
        outFile.close();
    }

    // Called by the world update start event
void ModelPush::OnUpdate(const common::UpdateInfo & /*_info*/)
    {
        // Get Sim time
//        common::Time  time = common::Time::GetWallTime();
        double sec = modelPush.time_sec + modelPush.time_nsec / MILLION;
        //cout << sec << endl;

//        double A = 4;
//        double T = 4 * M_PI;
//        double w = 2 * M_PI / T;
//        double vel = A*w*cos(w*sec);


        double r_m = 20.d;
        double v_km_h = 5.d;
        double v_m_s = v_km_h * 1000.d / 3600.d;
        double w_rad_s = v_m_s / r_m;

        double xo_r = r_m*sqrt(2.d);
        double xo_l = -r_m*sqrt(2.d);

//        int num_samples = 60;
        double phi = (3.d* (double) M_PI)/4.d + sqrt(2.d)/2.d;
        double phi_2 = (double) M_PI/4.d + 2.d*acos(xo_r/r_m - sqrt(2.d)/2.d) - (3*sqrt(2.d))/2;
//        double t_0 = ((7*M_PI)/4 - acos(xo_r/r_m - sqrt(2.d)/2) + sqrt(2.d)/2)/w_rad_s;
//        double t_0_2 = 2*t_0 + r_m*sqrt(2.d)/v_m_s;
        double t_0 = ((7*M_PI)/4 - acos(xo_r/r_m - sqrt(2.f)/2) + sqrt(2.f)/2)/w_rad_s;
        double t_0_1 = t_0 + r_m*sqrt(2.f)/v_m_s;
        double t_0_2 = 2*t_0 + r_m*sqrt(2.f)/v_m_s;

        double total_time = 2*t_0 + 3*10*sqrt(2.d)/v_m_s;
//        double interval = total_time/num_samples;

        double vx = 0,vy = 0, x = 0, y = 0;

        if (this->first){
            sec_init = sec;
            first = false;
            this->nloops++;
            cout << total_time << endl;
        }


         double t = sec - sec_init /*- this->nloops * total_time*/;


//        if (t < r_m/2*sqrt(2.d)/v_m_s)  // First slope rectilinear
//        {
//            vx = sqrt(v_m_s*v_m_s/2.d);
//            vy = sqrt(v_m_s*v_m_s/2.d);
//        }
//        else if (t < t_0)   // First circumference
//        {
//            vx = -r_m*w_rad_s*sin(w_rad_s*t-phi);
//            vy = -r_m*w_rad_s*cos(w_rad_s*t-phi);
//        }
//        else if (t < t_0 + r_m*sqrt(2.d) / v_m_s)   // Second slope rectilinear
//        {
//            vx=-sqrt(v_m_s*v_m_s/2.d);
//            vy=sqrt(v_m_s*v_m_s/2.d);
//        }
//        else if (t < 2*t_0 + (r_m / 2 *sqrt(2.d))/v_m_s)    // Second circumference
//        {
//            vx=-r_m *w_rad_s*sin(w_rad_s*t+phi_2);
//            vy=r_m*w_rad_s*cos(w_rad_s*t+phi_2);
//        }
//        else if (t < 2*t_0 + r_m*sqrt(2.d)/v_m_s)   // Third slope rectilinear
//        {
//            vx=sqrt(v_m_s*v_m_s/2.d);
//            vy=sqrt(v_m_s*v_m_s/2.d);
//        }

         x_old = x;
         y_old = y;

         double dot, det, angle;

         if (t < r_m/2*sqrt(2.f)/v_m_s) // First slope rectilinear
         {
             x=v_m_s*t;
             y=v_m_s*t;
             vx = sqrt(v_m_s*v_m_s/2.f);
             vy = sqrt(v_m_s*v_m_s/2.f);
             angle = 135 * M_PI / 180;
         }
         else if (t < t_0) // First circumference
         {

             x = xo_r + r_m * cos(-w_rad_s*t+phi);
             y = r_m*sin(-w_rad_s*t+phi);
             vx = -r_m*w_rad_s*sin(w_rad_s*t-phi);
             vy = -r_m*w_rad_s*cos(w_rad_s*t-phi);
             dot = vx;      // dot product
             det = vy;     // determinant
             angle = atan2(det, dot) + M_PI / 2;  // atan2(y, x) or atan2(sin, cos)
         }
         else if (t < t_0 + r_m*sqrt(2.f) / v_m_s) // Second slope rectilinear
         {

             x=-v_m_s*(t - t_0_1) - r_m/2*sqrt(2.f);
             y=v_m_s*(t - t_0_1) + r_m/2*sqrt(2.f);
             vx=-sqrt(v_m_s*v_m_s/2.f);
             vy=sqrt(v_m_s*v_m_s/2.f);
             angle = -135 * M_PI / 180;
         }
         else if (t < 2*t_0 + (r_m / 2 *sqrt(2.f))/v_m_s) // Second circumference
         {
             x=-xo_r + r_m * cos(w_rad_s*t+phi_2);
             y=r_m*sin(w_rad_s*t+phi_2);
             vx=-r_m *w_rad_s*sin(w_rad_s*t+phi_2);
             vy=w_rad_s*r_m*cos(w_rad_s*t+phi_2);
             dot = vx;      // dot product
             det = vy;     // determinant
             angle = atan2(det, dot) + M_PI / 2;  // atan2(y, x) or atan2(sin, cos)
         }
         else if (t < 2*t_0 + r_m*sqrt(2.f)/v_m_s) // Third slope rectilinear
         {
             x=v_m_s*(t - t_0_2);
             y=v_m_s*(t - t_0_2);
             vx=sqrt(v_m_s*v_m_s/2.f);
             vy=sqrt(v_m_s*v_m_s/2.f);
             angle = 135 * M_PI / 180;
         }

         if (t > total_time*(3.d/4.d) && x == 0 && y == 0){
             first = true;
         }

         math::Pose p_model = this->model->GetRelativePose();

        // Apply a small linear velocity to the model.
//        if (t < total_time){
//            this->model->SetLinearVel(math::Vector3(vx, vy, 0));
//            this->model->SetLinearAccel(math::Vector3(0, 0, 0));

            math::Pose pose;
            pose.pos.x = x;
            pose.pos.y = y;
            pose.pos.z = -0.5;

//            if (angle < 0)  angle = 2*M_PI + angle;

//            if (dot < 0 || det < 0)    angle = - angle;


//            cout << "angle: " << angle * 180 / M_PI << endl;




            geometry_msgs::Quaternion quat = tf::createQuaternionMsgFromRollPitchYaw(0, 0, angle);
            pose.rot.x = quat.x;
            pose.rot.y = quat.y;
            pose.rot.z = quat.z;
            pose.rot.w = quat.w;

            this->model->SetRelativePose(pose);

//            tf::Quaternion rotation_rpy;
//            rotation_rpy.setRPY(x,y, theta);
//            geometry_msgs::Quaternion rotation_quat;
//            tf::quaternionTFToMsg(rotation_rpy, rotation_quat);

            if ((t < 2*t_0 + (r_m / 2 *sqrt(2.f))/v_m_s)  && (t > t_0 + r_m*sqrt(2.f) / v_m_s)) // Second circumference
            {
                droneMsgsROS::dronePositionRefCommandStamped pose_stack;
                pose_stack.header.stamp = ros::Time::now();
                x=-xo_r + r_m * cos(w_rad_s*(t+1.65)+phi_2);
                y=r_m*sin(w_rad_s*(t+1.65)+phi_2);
                pose_stack.position_command.x = x-0.7;
                pose_stack.position_command.y = y+0.7;
                pose_stack.position_command.z = 2.3  - (5*t - t_0 + r_m*sqrt(2.f) / v_m_s);
                dronePositionPub.publish(pose_stack);
            }
            else{
                droneMsgsROS::dronePositionRefCommandStamped pose_stack;
                pose_stack.header.stamp = ros::Time::now();
                pose_stack.position_command.x = -v_m_s*r_m/2*sqrt(2.f)/v_m_s;
                pose_stack.position_command.y = v_m_s*r_m/2*sqrt(2.f)/v_m_s;
                pose_stack.position_command.z = 2.3;
                dronePositionPub.publish(pose_stack);
            }

//        }

//        math::Vector3 v_model = this->model->GetRelativeLinearVel();


//        cout << "vx: " << vx << "\tvy: " << vy << endl;
//        cout << "|v_commanded|: " << sqrt(vx*vx + vy*vy)  << endl;
//        cout << "|v_model|: " << sqrt(v_model[0]*v_model[0] + v_model[1]*v_model[1])  << endl;

        // Output to file
//        outFile.open ("/home/alejandro/mbzirc_speeds.csv", ios::out | ios::ate | ios::app) ;
//        outFile << t << "," << p_model.pos.x << "," << p_model.pos.y << "," << vx << "," << vy <<  endl;
//        outFile.close();


        // Get world pose
        //       math::Pose pose = this->model->GetWorldPose();
        //       math::Vector3 pos = pose.pos;
        //       cout << "Moving Platform current pose" << endl;
        //       cout << "x: " << pos.x << "y: " << pos.y << "z: " << pos.z << endl;



    }


void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg){

    ros::Time time = msg->clock;

    modelPush.time_sec = time.sec;
    modelPush.time_nsec = time.nsec;

    //cout << "time secs: " << modelPush.time_sec << endl;
}
