#!/bin/bash
pkill -9 gzserver
pkill -9 gzclient
pkill -9 gazebo
xdotool search --class "terminal" | while read id
do
      xdotool windowactivate "$id" &>/dev/null
      xdotool key ctrl+shift+q
      sleep 0.2
done 
