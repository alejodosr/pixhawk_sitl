#Set Kbmap to English!
setxkbmap -layout es
setxkbmap -layout en

TERM_PID=$(echo `ps -C gnome-terminal -o pid= | head -1`) # get first gnome-terminal's PID
WID=$(wmctrl -lp | awk -v pid=$TERM_PID '$3==pid{print $1;exit;}') # get window id

#create tab
xdotool windowfocus $WID
xdotool key ctrl+shift+t
xdotool sleep 1

#launch roscore
xdotool windowfocus $WID
xdotool type "roscore"
xdotool key Return
xdotool sleep 1  

#create tab
xdotool windowfocus $WID
xdotool key ctrl+shift+t
xdotool sleep 1

#start SITL simulation
xdotool windowfocus $WID
xdotool type "cd ~/pixhawk_sitl/Firmware"
xdotool key Return
xdotool sleep 1
xdotool windowfocus $WID 
xdotool type "no_sim=1 make posix_sitl_default gazebo"
xdotool key Return
xdotool sleep 1

#create tab
xdotool windowfocus $WID
xdotool key ctrl+shift+t
xdotool sleep 1

#Start gazebo
xdotool windowfocus $WID
xdotool type "cd ~/pixhawk_sitl/sitl_gazebo/worlds"
xdotool key Return
xdotool sleep 1
xdotool windowfocus $WID
xdotool type "rosrun gazebo_ros gazebo iris_hokuyo_IMAV_vs.world"
xdotool key Return
xdotool sleep 7

#Activate terminal
xdotool windowactivate $(xdotool search --onlyvisible --class gnome-terminal)

#create tab
xdotool windowfocus $WID
xdotool key ctrl+shift+t
xdotool sleep 1

#Start aerostack
xdotool windowfocus $WID
xdotool type "cd $AEROSTACK_STACK											/launchers/pixhawk_simulation_launchers"
xdotool key Return
xdotool sleep 1
xdotool windowfocus $WID
xdotool type "./pixhawk_simulation.sh"
xdotool key Return
xdotool sleep 3

#create tab
xdotool windowfocus $WID
xdotool key ctrl+shift+t
xdotool sleep 1

#Start EKF and ARM
xdotool windowfocus $WID
xdotool type "rosservice call /drone5/mavros/cmd/arming "
xdotool key "shift+2"
xdotool type "value: true"
xdotool key "shift+2"
xdotool key Return
xdotool sleep 1
xdotool windowfocus $WID
xdotool type "rosservice call /drone5/mavros/set_mode "
xdotool key "shift+2"
xdotool type "base_mode: 0 "
xdotool key Return
xdotool type "custom_mode: 'OFFBOARD'"
xdotool key "shift+2"
xdotool key Return
xdotool sleep 1
xdotool windowfocus $WID
xdotool type "rosservice call /drone5/droneRobotLocalizationROSModuleNode/start"
xdotool key Return
xdotool sleep 1

#Launch VS node
xdotool windowfocus $WID
xdotool type "roslaunch droneVSFollowingROSModule droneVSFollowingROSModule.launch "
xdotool key Return
xdotool sleep 1

#create tab
xdotool windowfocus $WID
xdotool key ctrl+shift+t
xdotool sleep 1

#Start VS node
xdotool windowfocus $WID
xdotool type "rosservice call /drone5/droneVSFollowing/start"

setxkbmap -layout es
