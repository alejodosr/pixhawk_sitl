/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef _GAZEBO_CAMERA_PLUGIN_HH_
#define _GAZEBO_CAMERA_PLUGIN_HH_

#include <string>

#include "gazebo/common/Plugin.hh"
#include "gazebo/sensors/CameraSensor.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/common/common.hh"
#include "gazebo/rendering/Camera.hh"
#include "gazebo/util/system.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/msgs/msgs.hh"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

#include "opticalFlow.pb.h"

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

#include <ros/ros.h>
#include <mavros_msgs/OpticalFlowRad.h>
#include <sys/time.h>

#include "sensor_msgs/Imu.h"
#include "sensor_msgs/Range.h"
#include "nav_msgs/Odometry.h"

#include "boost/interprocess/sync/interprocess_mutex.hpp"
#include <boost/thread/thread.hpp>
#include <tf/tf.h>

#include <rosgraph_msgs/Clock.h>

#define SEARCH_SIZE     4
#define BUFFER_SIZE     100
#define THR_DIST        0.5

using namespace cv;
using namespace std;

namespace gazebo
{
  class GAZEBO_VISIBLE CameraPlugin : public SensorPlugin
  {
    public: CameraPlugin();

    /// \brief Destructor
    public: virtual ~CameraPlugin();

    public: virtual void Load(sensors::SensorPtr _sensor, sdf::ElementPtr _sdf);

    public: virtual void OnNewFrame(const unsigned char *_image,
                              unsigned int _width, unsigned int _height,
                              unsigned int _depth, const std::string &_format);

    public:
      void estimatedPoseCallback(const sensor_msgs::Range::ConstPtr& msg);
      void gyroCallback(const sensor_msgs::Imu::ConstPtr& msg);
      void getSimulationClockTime(const rosgraph_msgs::Clock::ConstPtr& msg);
//      void configureCallback(dynamic_tutorials::TutorialsConfig &config, uint32_t level);

    protected: unsigned int width, height, depth;
    protected: std::string format, camera_name;

    protected: sensors::CameraSensorPtr parentSensor;
    protected: rendering::CameraPtr camera;

    private: event::ConnectionPtr newFrameConnection;

    private: 
	 vector<Point2f> featuresPrevious;
     vector<Point2f> featuresCurrent;
     vector<uchar> featuresFound;

     Mat err;

	 std::string namespace_;

    ros::NodeHandle *node_handle_;
    ros::Publisher opticalflow_pub;
    image_transport::Publisher camera_pub;
    ros::Subscriber estimated_pose_sub;
    ros::Subscriber gyro_sub;
    ros::Subscriber clock_sub;

    boost::interprocess::interprocess_mutex mutex;
    boost::interprocess::interprocess_mutex mutex2;
    cv_bridge::CvImagePtr img_buffer_ptr;
    boost::thread image_process_thread;

    Mat frame;
    Mat prev_frame;

    ros::Time time;

    cv::KalmanFilter KF;
    cv::Mat measurement;
//    cv::KalmanFilter KF_2;
//    cv::Mat measurement_2;

    bool newImageReceived();
    void getLastImageFromBuffer();

    void image_process(void);



    double altitude, x_rate, y_rate, z_rate, current_x_rate, current_y_rate, current_altitude;


    ros::Time current_time;
    ros::Time  previous_time;

     int maxfeatures;
     double qualityLevel;
     double minDistance;
     int blockSize;
     bool useHarrisDetector;
     double k;

  };
}

#endif
