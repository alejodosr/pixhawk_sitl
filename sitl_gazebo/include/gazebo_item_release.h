#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <math.h>
#include <ros/ros.h>
#include <rosgraph_msgs/Clock.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>

#define MILLION 1E9

using namespace std;

namespace gazebo
{
class ModelPush : public ModelPlugin
{

/// \brief Constructor
    public: ModelPush();

    /// \brief Destructor
    public: virtual ~ModelPush();

private: void itemReleasecb(const std_msgs::String::ConstPtr &msg);

public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf);

    // Called by the world update start event
public: void OnUpdate(const common::UpdateInfo & /*_info*/);

    // Pointer to the model
private: physics::ModelPtr model;

         std::string joint_name_left_;
         std::string joint_name_right_;
         physics::JointPtr joint_left_;
         physics::JointPtr joint_right_;

         std::string joint_name_;
         physics::JointPtr joint_;


    // Pointer to the update event connection
private: event::ConnectionPtr updateConnection;

private:
    ros::NodeHandle *node_handle_;
    ros::Subscriber moving_sub;
    ros::Subscriber release_sub;
    std::string namespace_;
public:
    double time_sec;
    double time_nsec;
};
}
