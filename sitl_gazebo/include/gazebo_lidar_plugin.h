/*
 * Copyright (C) 2012-2014 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/*
 * Desc: Ray Plugin
 * Author: Nate Koenig mod by John Hsu
 */

/*
* Extended Plugin for using both Optical Flow and LIDAR information (px4Flow)
* Author: Alejandro Rodríguez Ramos
* Date: 27th January, 2016
* Email: alejandro.dosr@gmail.com
*
* Description: This extention lets the lidar plugin use the Optical Flow information
* in order to calculate the Gazebo Model velocity. Optical Flow information is retrieved through
* ROS messages communication. Finally, px4Flow calculations are published via ROS Publisher.
*
* File: gazebo_lidar_pkugin.h
*
*/


/* Includes */
#ifndef _GAZEBO_RAY_PLUGIN_HH_
#define _GAZEBO_RAY_PLUGIN_HH_

#include "gazebo/common/Plugin.hh"
#include "gazebo/sensors/SensorTypes.hh"
#include "gazebo/sensors/RaySensor.hh"
#include "gazebo/util/system.hh"

#include <ros/ros.h>
#include <sensor_msgs/Range.h>
#include <mavros_msgs/OpticalFlowRad.h>
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"

#include "lidar.pb.h"

namespace gazebo
{
  /// \brief A Ray Sensor Plugin
  class GAZEBO_VISIBLE RayPlugin : public SensorPlugin
  {
    /// \brief Constructor
    public: RayPlugin();

    /// \brief Destructor
    public: virtual ~RayPlugin();

    /// \brief Update callback
    public: virtual void OnNewLaserScans();

    /// \brief Load the plugin
    /// \param take in SDF root element
    public: void Load(sensors::SensorPtr _parent, sdf::ElementPtr _sdf);

    /// \brief Pointer to parent
    protected: physics::WorldPtr world;

    /// \brief The parent sensor
    private: 
      sensors::RaySensorPtr parentSensor;
//      transport::NodePtr node_handle_;
//      transport::PublisherPtr lidar_pub_;
      std::string namespace_;

     ros::NodeHandle *node_handle_;
     ros::Publisher lidar_pub;
     ros::Publisher pose_pub;
     ros::Subscriber lidar_sub;

    /// \brief The connection tied to RayPlugin::OnNewLaserScans()
    private: 
      event::ConnectionPtr newLaserScansConnection;
//      lidar_msgs::msgs::lidar lidar_message;
public:
      float range;
      ros::Publisher px4Flow_pub;
  };
}
#endif
